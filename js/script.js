function showContactForm() {
  var button = document.getElementById('contact-form-button');
  var button2hide = document.getElementById('contact-form-button');
  var container = document.getElementById('contact-form-wrapper');
  container.style.display = 'block';
  button2hide.style.display = 'none';
  return false;
}

function hideContactForm() {
  var button2hide = document.getElementById('contact-form-button');
  var container = document.getElementById('contact-form-wrapper');
  container.style.display = 'none';
  button2hide.style.display = 'block';
  return false;
}

function tabsScroll(e){ 
e.stopImmediatePropagation();
}

function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}